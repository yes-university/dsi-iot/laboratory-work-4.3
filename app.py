def factorial(n):
    return 1 if (n <= 1) else n * factorial(n - 1)


def permutations(n, k):
    return factorial(n) / factorial(n - k)


def arrangements(n, k):
    return factorial(n) / (factorial(n - k) * factorial(k))


def combinations(n, k):
    return arrangements(n + k - 1, k)


def combinations_with_repetition(n, k):
    return n**k


def main():
    n = int(input("Enter n: "))
    k = int(input("Enter k: "))
    print("Permutations: ", permutations(n, k))
    print("Arrangements: ", arrangements(n, k))
    print("Combinations: ", combinations(n, k))
    print("Combinations with repetition: ", combinations_with_repetition(n, k))


if __name__ == "__main__":
    main()
